## Laravel To Do List App
Building a basic to do application using the Laravel PHP Framework.
In order to run the application you need to setup the database configuration file located at `<app-folder>/app/config/database.php`

Then you need to enter into the app folder via terminal and you need to perform these two commands:

* `artisan migrate`
* `artisan db:seed`

The first one creates the database and the second one populates it.
If you want to see the application running do this command:
`artisan serve` then access via browser at the shown url.

Based on the [YouTube video series](http://www.youtube.com/playlist?list=PLfdtiltiRHWGH8AngyP6cp525_R_NExcR) of [phpacademy.org](https://phpacademy.org/)

### Contributing to this repository

All issues and pull requests should be filed on the [repository](https://bitbucket.org/gabrieleciech/laravel-to-do-list-app).
