<htmml>
	<head>
		<title>Our todo Application</title>
		<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}">
	</head>

	<body>
		<div class="container">
			@yield('content')
		</div>
	</body>
</htmml>
