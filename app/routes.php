<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
// Bind URL parameters to object instances
// This binding is the same as writing:
// Route::model('item', 'Item'); --> the first parameter is the param name, the second one the name of the model
Route::bind('item', function($value, $route) {
	return Item::where('id', $value)->first();
});


// Home
Route::get('/', array('as' => 'home', 'uses' => 'HomeController@getIndex'))->before('auth');
Route::post('/', array('uses' => 'HomeController@postIndex'))->before('csrf');

// New task
Route::get('/new', array('as' => 'new', 'uses' => 'HomeController@getNew'))->before('auth');
Route::post('/new', array('uses' => 'HomeController@postNew'))->before('csrf');

// Delete task
Route::get('/delete/{item}', array('as' => 'delete', 'uses' => 'HomeController@getDelete'))->before('auth');

// Login
Route::get('/login', array('as' => 'login', 'uses' => 'AuthController@getLogin'))->before('guest');
Route::post('login', array('uses' => 'AuthController@postLogin'))->before('csrf');
