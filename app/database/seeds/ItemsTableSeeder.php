<?php

class ItemsTableSeeder extends Seeder {

	public function run() {
		DB::table('items')->delete();

		$items = array(
			array(
				'owner_id' => 1,
				'name' => 'Pick up milk',
				'done' => FALSE
			),
			array(
				'owner_id' => 1,
				'name' => 'Walk the dogs',
				'done' => TRUE
			),
			array(
				'owner_id' => 1,
				'name' => 'Cook dinner',
				'done' => FALSE
			)
		);

		DB::table('items')->insert($items);
	}
}
